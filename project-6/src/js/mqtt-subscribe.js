var mqtt = require('mqtt');
// free broker: https://www.emqx.com/en/mqtt/public-mqtt5-broker
const HOST = 'ws://broker.emqx.io:8083/mqtt'
const TOPICS = ['octoprint/temperature/bed', 'octoprint/temperature/tool0'];

const options = {
    keepalive: 5,
    clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
    protocolId: 'MQTT',
    protocolVersion: 4,
    clean: true,
};

// data parameter
var DATA = {timestamp: 0,
            bed_data: 0,
            tool_data: 0};

// create client
var client = mqtt.connect(HOST, options);

client.on('message', function(topic, message, packet){
    console.log("topic: "+ topic);
    console.log("message: "+ message + "\n");

    var msg = JSON.parse(message.toString());
    console.log(msg);

    DATA['timestamp'] = msg._timestamp;

    if(topic == TOPICS[0]) {
        DATA['bed_temp'] = msg.bed_data;
        //document.getElementById("originFarmLatitude").value = msg.actual;
    }
    if(topic == TOPICS[1]) {
        DATA['tool_temp'] = msg.tool_data;
       // document.getElementById("originFarmLongitude").value = msg.actual;
    }
});

// connect to broker
client.on("connect", function() {	
    console.log("connected");
});

//handle errors
client.on("error", function(error){
    console.log("Can't connect" + error);
    process.exit(1);
});

// subscribe to topics
client.subscribe(TOPICS, {qos:1});