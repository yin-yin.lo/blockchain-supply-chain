var App = {
    web3Provider: null,
    contracts: {},
    emptyAddress: "0x0000000000000000000000000000000000000000",
    sku: 0,
    upc: 0,
    metamaskAccountID: "0x0000000000000000000000000000000000000000",
    ownerID: "0x0000000000000000000000000000000000000000",
    originFarmerID: "0x0000000000000000000000000000000000000000",
    originFarmName: null,
    originFarmInformation: null,
    originFarmLatitude: null,
    originFarmLongitude: null,
    productNotes: null,
    productPrice: 0,
    distributorID: "0x0000000000000000000000000000000000000000",
    retailerID: "0x0000000000000000000000000000000000000000",
    consumerID: "0x0000000000000000000000000000000000000000",

    init: async function () {
        App.readForm();
        /// Setup access to blockchain
        return await App.initWeb3();
    },

    readForm: function () {
        App.sku = $("#sku").val();
        App.upc = $("#upc").val();
        App.ownerID = $("#ownerID").val();
        App.originFarmerID = $("#originFarmerID").val();
        App.originFarmName = $("#originFarmName").val();
        App.originFarmInformation = $("#originFarmInformation").val();
        App.originFarmLatitude = $("#originFarmLatitude").val();
        App.originFarmLongitude = $("#originFarmLongitude").val();
        App.productNotes = $("#productNotes").val();
        App.productPrice = $("#productPrice").val();
        App.distributorID = $("#distributorID").val();
        App.retailerID = $("#retailerID").val();
        App.consumerID = $("#consumerID").val();

        console.log(
            "SKU: ",            App.sku, ";\n",
            "UPC: ",            App.upc, ";\n",
            "Owner ID: ",       App.ownerID, ";\n",
            "Farmer ID: ",      App.originFarmerID, "; \n",
            "Farm Name: ",      App.originFarmName, "; \n",
            "Farm Info: ",      App.originFarmInformation, ";  \n",
            "3D Printer Bed Temperature: ",  App.originFarmLatitude, "; \n",  // TODO: change back to Farm Latidude and Longitude
            "3D Printer Tool Temperaturee: ", App.originFarmLongitude, ";\n",
            "Product Notes: ",  App.productNotes, ";   \n",
            "Product Price: ",  App.productPrice, ";   \n",
            "Distributer ID: ", App.distributorID, ";  \n",
            "Retailer ID: ",    App.retailerID, "; \n",
            "Consumer ID: ",    App.consumerID
        );
    },

    initWeb3: async function () {
        /// Find or Inject Web3 Provider
        /// Modern dapp browsers...
        if (window.ethereum) {
            App.web3Provider = window.ethereum;
            try {
                // Request account access
                await window.ethereum.enable();
            } catch (error) {
                // User denied account access...
                console.error("User denied account access")
            }
        }
        // Legacy dapp browsers...
        else if (window.web3) {
            App.web3Provider = window.web3.currentProvider;
        }
        // If no injected web3 instance is detected, fall back to Ganache
        else {
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
            console.log("Getting Here!");
        } 

        App.web3Provider=web3.currentProvider;

        App.getMetaskAccountID();

        return App.initSupplyChain();
    }, 

    getMetaskAccountID: function () {
        var web3 = new Web3(App.web3Provider);

        // Retrieving accounts
        web3.eth.getAccounts(function (err, res) {
            if (err) {
                console.log('Error:', err);
                return;
            }
            console.log('getMetaskID:', res);
            App.metamaskAccountID = res[0];

        })
    },

    initSupplyChain: function () {
        /// Source the truffle compiled smart contracts
        var jsonSupplyChain = '../../build/contracts/SupplyChain.json';

        /// JSONfy the smart contracts
        $.getJSON(jsonSupplyChain, function (data) {
            console.log('data', data);
            var SupplyChainArtifact = data;
            App.contracts.SupplyChain = TruffleContract(SupplyChainArtifact);
            App.contracts.SupplyChain.setProvider(App.web3Provider);

            App.fetchItemBufferOne();
            App.fetchItemBufferTwo();
            App.fetchEvents();

        });

        return App.bindEvents();
    },

    bindEvents: function () {
        $(document).on('click', App.handleButtonClick);
    },

    handleButtonClick: async function (event) {
        event.preventDefault();

        App.getMetaskAccountID();
        App.readForm();

        var processId = parseInt($(event.target).data('id'));
        console.log('processId', processId);

        switch (processId) {
            case 1:
                return await App.harvestItem(event);
                break;
            case 2:
                return await App.processItem(event);
                break;
            case 3:
                return await App.packItem(event);
                break;
            case 4:
                return await App.sellItem(event);
                break;
            case 5:
                return await App.buyItem(event);
                break;
            case 6:
                return await App.shipItem(event);
                break;
            case 7:
                return await App.receiveItem(event);
                break;
            case 8:
                return await App.purchaseItem(event);
                break;
            case 9:
                App.modelFarmButton(event);
                return await App.fetchItemBufferOne(event);
                break;
            case 10:
                App.modelProductButton(event);
                return await App.fetchItemBufferTwo(event);
                break;
            case 11:
                App.updateMqttData(event);
                break;
        }
    },

    harvestItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.harvestItem(
                App.upc,                    // uint _upc,
                App.metamaskAccountID,      // address payable _originFarmerID,
                App.originFarmName,     
                App.originFarmInformation,
                App.originFarmLatitude,
                App.originFarmLongitude,
                App.productNotes, 
                { from: App.metamaskAccountID }
            );
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('harvestItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    processItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.processItem(App.upc, { from: App.metamaskAccountID });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('processItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    packItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.packItem(App.upc, { from: App.metamaskAccountID });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('packItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    sellItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));
        var web3 = new Web3(App.web3Provider);
       
        App.contracts.SupplyChain.deployed().then(function(instance) {
            const productPrice = web3.utils.toWei('1', "ether");
            console.log('productPrice', productPrice);
            return instance.sellItem(App.upc, App.productPrice, {from: App.metamaskAccountID});
        }).then(function(result) {
            $("#ftc-item").text(result);
            console.log('sellItem',result);
        }).catch(function(err) {
            console.log(err.message);
        });
    },

    buyItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));
        var web3 = new Web3(App.web3Provider);

        App.contracts.SupplyChain.deployed().then(function (instance) {
            const walletValue = web3.utils.toWei('3', "ether");
            return instance.buyItem(App.upc, { from: App.metamaskAccountID, value: walletValue });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('buyItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    shipItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.shipItem(App.upc, { from: App.metamaskAccountID });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('shipItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    receiveItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.receiveItem(App.upc, { from: App.metamaskAccountID });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('receiveItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    purchaseItem: function (event) {
        event.preventDefault();
        var processId = parseInt($(event.target).data('id'));
        var web3 = new Web3(App.web3Provider);
        
        App.contracts.SupplyChain.deployed().then(function (instance) {
            const walletValue = web3.utils.toWei('1', "ether");
            return instance.purchaseItem(App.upc, { from: App.metamaskAccountID, value: walletValue });
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('purchaseItem', result);
        }).catch(function (err) {
            console.log(err.message);
        });
    },

    // Fetches Farm Data 
    fetchItemBufferOne: function () {
        App.upc = $('#upc').val();
        console.log('upc', App.upc);
        var log = document.getElementById('demo-farm');

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.fetchItemBufferOne.call(App.upc);
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('fetchItemBufferOne', result);

            if(result.itemUPC == 0) {
                result.originFarmerID = '/';
                result.originFarmName = '/';
                result.originFarmInformation = '/';
                result.originFarmLatitude = '/';
                result.originFarmLongitude ='/';
            } else {
                result.originFarmerID = App.originFarmerID;
                result.originFarmName = App.originFarmName;
                result.originFarmInformation = App.originFarmInformation;
                result.originFarmLatitude = App.originFarmLatitude;
                result.originFarmLongitude = App.originFarmLongitude;
            }

            log.innerHTML = 
                "<i class='fas fa-info-circle'></i> <b>Product UPC:</b> " 
                    + result.itemUPC + "<br />" +
                "<i class='fas fa-tractor'></i> <b>Farmer ID:</b> " 
                    + result.originFarmerID + "<br />" +               // TODO: App zu result ändern
                "<i class='fas fa-tractor' style='color: transparent'></i> <b>Farm Name:</b> " 
                    + result.originFarmName + "<br />" +               // TODO: App zu result ändern
                "<i class='fas fa-tractor' style='color: transparent'></i> <b>Farm Information:</b> " 
                    + result.originFarmInformation + "<br />" +        // TODO: App zu result ändern
                "<i class='fas fa-tractor' style='color: transparent'></i> <b>3D Printer Bed Temperature:</b> "  // TODO: change back to Farm Latitude
                    + result.originFarmLatitude + "<br />" +           // TODO: App zu result ändern
                "<i class='fas fa-tractor' style='color: transparent'></i> <b3D Printer Tool Temperature:</b> " // TODO: change back to Farm Longitude
                    + result.originFarmLongitude;
        }).catch(function (err) {
            console.log(err.message);
        });
    },


    // model up-up window for farm information after pushing button    
    modelFarmButton: function () {
        // Get the modal, button, span
        var modal = document.getElementById("farmDataModal");
        var btn = document.getElementsByClassName("btn-fetchOne");
        var span = document.getElementsByClassName("close1")[0];

        // When the user clicks on the button, open the modal
        modal.style.display = "block";
        span.onclick = function() {
            modal.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    },


    // Fetches Product Data
    fetchItemBufferTwo: function () { 
        App.upc = $('#upc').val();
        var log = document.getElementById('demo-product');

        App.contracts.SupplyChain.deployed().then(function (instance) {
            return instance.fetchItemBufferTwo.call(App.upc);
        }).then(function (result) {
            $("#ftc-item").text(result);
            console.log('fetchItemBufferTwo', result);
            console.log('product ID:', typeof result.productID)
            
            if(result.productID == 0) {
                result.productNotes = '/';
                result.distributorID = '/';
                result.retailerID = '/';
                result.consumerID = '/';
            } else {
                result.distributorID = App.distributorID;
                result.retailerID = App.retailerID;
                result.consumerID = App.consumerID;
            }

            log.innerHTML = 
                "<i class='fas fa-info-circle'></i> <b>Product ID:</b> " 
                    + result.productID +                 
                "<i class='fas fa-info-circle' style='color: transparent; padding-left: 15em;'></i><b>Product UPC:</b> " 
                    + result.itemUPC + "<br />" +
                "<hr>" +
                "<i class='fas fa-mug-hot'></i> <b>Product Name:</b> " 
                    + result.productNotes + "<br />" +
                "<i class='fab fa-ethereum'></i> <b>Product Price:</b> " 
                    + result.productPrice + "  ETH <br />" +
                "<i class='fas fa-truck'></i> <b>Distributor ID:</b> " 
                    + result.distributorID + "<br />" +                        // TODO: App zu result ändern
                "<i class='fas fa-store'></i> <b>Retailer ID:</b> " 
                    + result.retailerID + "<br />" +                           // TODO: App zu result ändern
                "<i class='fas fa-user'></i> <b>Consumer ID:</b> " 
                    + result.consumerID;                                       // TODO: App zu result ändern
        }).catch(function (err) {
            console.log(err.message);
        });
    },


    // model up-up window for product information after pushing button
    modelProductButton: function () {
        // Get the modal, button, span
        var modal = document.getElementById("productDataModal");
        var btn = document.getElementsByClassName("btn-fetchTwo");
        var span = document.getElementsByClassName("close2")[0];

        // When the user clicks on the button, open the modal
        modal.style.display = "block";
        span.onclick = function() {
            modal.style.display = "none";
        }
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    },


    // get and update mqtt data for 3D printer
    updateMqttData: function () {

        // free broker: https://www.emqx.com/en/mqtt/public-mqtt5-broker
        const HOST = 'wss://broker.emqx.io:8084/mqtt'
        const TOPICS = ['octoprint/temperature/bed', 'octoprint/temperature/tool0'];

        const options = {
            keepalive: 1,
            clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
            protocolId: 'MQTT',
            protocolVersion: 4,
            clean: true,
        };

        // data parameter
        var DATA = {timestamp: 0,
                    bed_data: 0,
                    tool_data: 0};

        // create client
        var client = mqtt.connect(HOST, options);

        client.on('message', function(topic, message, packet){
            console.log("topic: "+ topic);
            console.log("message: " + message);

            var msg = JSON.parse(message.toString());
            
            DATA['timestamp'] = msg._timestamp;

            if(topic == TOPICS[0]) {
                DATA['bed_data'] = msg.actual;
                document.getElementById("originFarmLatitude").value = msg.actual;
            }
            if(topic == TOPICS[1]) {
                DATA['tool_data'] = msg.actual;
                document.getElementById("originFarmLongitude").value = msg.actual;
            }

            if(DATA['bed_data'] != 0 || DATA['tool_data'] != 0) {
                client.end();
            }
        });

        // connect to broker
        client.on("connect", function() {	
            console.log("connected");
        });

        //handle errors
        client.on("error", function(error){
            console.log("Can't connect" + error);
            process.exit(1);
        });

        // subscribe to topics
        client.subscribe(TOPICS, {qos:2});
    },


    fetchEvents: function () {
        if (typeof App.contracts.SupplyChain.currentProvider.sendAsync !== "function") {
            App.contracts.SupplyChain.currentProvider.sendAsync = function () {
                return App.contracts.SupplyChain.currentProvider.send.apply(
                    App.contracts.SupplyChain.currentProvider,
                    arguments
                );
            };
        }

        App.contracts.SupplyChain.deployed().then(
            function (instance) {
                var events = instance.allEvents(function (err, log) {
                    if (!err) { 
                        $("#ftc-events").append('<li>' + log.event + ' - ' + log.transactionHash + '</li>');
                    }
                });
            }
        ).catch(function (err) {
            console.log(err.message);
        });

    }
};

$(function () {
    $(window).load(function () {
        App.init();
        console.log('App init')
    });
});
