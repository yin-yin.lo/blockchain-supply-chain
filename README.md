# Blockchain Supply Chain Solution - Coffee

## Description

This project aims to solve the problem of tracking the full supply chain from the production of the coffee beans to the consumer hands. This repository containts an Ethereum DApp that demonstrates a Supply Chain flow.

This document uses UML to describre the general structure and functions that this Smart Contract offers to the actors:

- Farmers
- Distributors
- Retailers
- Consumers

The asset (coffee beans) goes through the following states from farmer to consumer: Harvested, Processed, Packed, ForSale, Sold, Shipped, Received and Purchased.

## Activity Diagram

This diagram shows the different actors and the interactions with the system.

![Activity Diagram](/project-6/images/activity_diagram.png)

## State Diagram

This diagram shows the different states of each object and the events or conditions that change those states.

![State Diagram](/project-6/images/state_diagram_final.png)


<!-- ## Data Modeling Diagram

This diagram shows the different Smart Contracts and the relation between them

![Data Modeling Diagram](/project-6/images/data_modeling_diagram.jpg) -->

## Included Technologies
* [Ethereum](https://ethereum.org/en/) - Ethereum is a decentralized platform that runs smart contracts
* [ganache-cli](https://nethereum.readthedocs.io/en/latest/ethereum-and-clients/ganache-cli/) - Ganache CLI uses ethereumjs to simulate full client behavior and make developing Ethereum applications faster, easier, and safer.
* [Truffle](https://www.trufflesuite.com/truffle) - A development environment, testing framework and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM), aiming to make life as a developer easier.
* [MetaMask](https://moonbeam.network/community/projects/metamask/#:~:text=MetaMask%20is%20an%20extension%20for%20accessing%20Ethereum-enabled%20decentralized,so%20that%20DApps%20can%20read%20from%20the%20blockchain.) - MetaMask is an extension for accessing Ethereum-enabled decentralized applications, or DApps, in your browser. The extension injects the Ethereum Web3 API into every website’s JavaScript context, so that DApps can read from the blockchain.

# Steps
The following instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1. [Install Prerequisites](#step-1-install-prerequisites)
2. [Set-up](#step-2-set-up)
3. [Launch Granache](#step-3-launch-granache)
4. [Create smart contract with Truffle](#step-4-create-smart-contract)
5. [Run DApp](#step-5-run-dapp)

Next time, start with *[Step 3](#step-3-launch-granache)*, because you only have to do step 1 and 2 once.

## Step 1: Install Prerequisites

* Install [node and npm](https://nodejs.org/de/download/) (v14.16.1)
* Install [ganache-cli](https://tutorialsdiary.com/ethereum-how-to-install-ganache-cli-windows/) (v6.12.2)
* Install [Truffle](https://www.trufflesuite.com/docs/truffle/getting-started/installation) (v5.3.5)
* Install [Truffle Contract](https://www.npmjs.com/package/@truffle/contract) (v4.3.5)
* Install [web3](https://www.npmjs.com/package/web3/v/1.3.3) (v1.3.3)
* Install [MetaMask](https://moonbeam.network/community/projects/metamask/#:~:text=MetaMask%20is%20an%20extension%20for%20accessing%20Ethereum-enabled%20decentralized,so%20that%20DApps%20can%20read%20from%20the%20blockchain.) - for your browser extension, create an account and save the generated text for later use.

## Step 2: Set-up

Clone this repository:
```
git clone https://git.rwth-aachen.de/yin-yin.lo/blockchain-supply-chain.git
```

Change directory to `project-6` folder and install all requisite npm packages (as listed in `package.json`):

```
cd project-6
npm install
```

## Step 3: Launche Ganache
Copy the following in cmd-line in directory `project-6`, but the text inside "" should be the text you generated yourself in MetaMasl:
```
ganache-cli -m "insect notice circle hawk delay village utility lift chicken exercise surface away" 
```
After that, your terminal should look like this:
![](/images/ganache-cli.png)

## Step 4: Create smart contract with Truffle
Open a new terminal and compile smart contracts in directory `project-6` with:
```
truffle.cmd compile
```
This will create the smart contract artifacts in folder `build\contracts`.

Migrate smart contracts to the locally running blockchain with ganache-cli. Copy following in cmd-line in directory `project-6`:
```
truffle.cmd migrate
```
After that, your terminal should look something like this:
![](/images/truffle_migrate.png)

Next, we test the smart contracts with:
```
truffle.cmd test
```
All 10 tests should pass as seen in the following:
![](/images/truffle_test.png)

## Step 5: Run DApp
Open new terminal and launch the DApp in directory `project-6` with:
```
npm run dev
```
This will lead you to a newly opened tab in your brower. 

__Note:__ Use Chrome, since other browsers will cause an extension error!